/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.exhaustivesearch;

/**
 *
 * @author acer
 */
public class Test_project {
    
    public static void main(String[] args) {
        int[] a = {10, 3, 5, 7, 11, 1, 17};
        showInput(a);
        Exhaustive exs = new Exhaustive(a);
        exs.process();
        exs.sum(); //sum and show result

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }

}
